package ru.b3nder;

public class Passport {
    private int series;
    private int number;

    public Passport(int series, int number) {
        this.series = series;
        this.number = number;
    }

    public int getSeries() {
        return series;
    }

    public int getNumber() {
        return number;
    }

    @Override
    public String toString() {
        return "Passport{" +
                "series=" + series +
                ", number=" + number +
                '}';
    }
}
