package ru.b3nder;

import java.util.*;

public class Decanat {
    private final List<Student> students = new ArrayList<>();
    private final List<Group> groups = new ArrayList<>();
    private static int studNumber = 1;

    public boolean addStudent(Group group, Student student) {
        if (group.addStudent(student)) {
            student.setGroup(group);
            return true;
        }
        return false;
    }

    public Student createStudent(String name, String surname, String patronymic, Gender gender, Passport passport) {
        Student student = new Student(name, surname, patronymic, studNumber++, gender, passport);
        students.add(student);
        return student;
    }

    public Group createGroup(String name) {
        Group group = new Group(name);
        groups.add(group);
        return group;
    }

    public List<Student> getAllStudents() {
        return students;
    }

    public List<Student> getStudentsByGroup(Group group) {
        return group.getStudents();
    }

    public boolean moveStudent(Student student, Group toGroup) {
        Group originalGroup = student.getGroup();
        if (originalGroup.equals(toGroup)) {
            System.out.println("Не удалось перевести студента из группы " + originalGroup + " в группу " + toGroup);
            return false;
        }
        originalGroup.removeStudent(student);

        toGroup.addStudent(student);
        student.setGroup(toGroup);
        return true;
    }

    public boolean kickStudent(Student student) {
        return students.remove(student);
    }

    public List<Group> getGroups() {
        return groups;
    }

    public int sizeOfGroups(Group group) {
        return getStudentsByGroup(group).size();
    }

    public boolean isStudentExist(String name, String surname, String patronymic) {
        for (Student student :
                students) {
            if (student.getName().equals(name) && student.getSurname().equals(surname) &&
            student.getPatronymic().equals(patronymic)) {
                return true;
            }
        }
        return false;
    }

    public Passport getPassData(Student student) {
        return student.getPassport();
    }

    public void printGroup(Group group) {
        List<Student> studentOfGroup = group.getStudents();
        Collections.sort(studentOfGroup);
        for (Student student :
                studentOfGroup) {
            System.out.println(student);
        }
    }
}
