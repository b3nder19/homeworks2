package ru.b3nder;

public class Main {
    public static void main(String[] args) {
        Decanat decanat = new Decanat();
        Student student = decanat.createStudent("Олег", "Тимошенко", "Владимирович",
                Gender.MALE, new Passport(1223, 123456));
        Student student2 = decanat.createStudent("Саня", "Григоренко", "Батькович",
                Gender.MALE, new Passport(4321, 654321));
        Group group = decanat.createGroup("Гражданское строительство");

        decanat.addStudent(group, student);
        decanat.addStudent(group, student2);

        decanat.printGroup(group);
    }
}
