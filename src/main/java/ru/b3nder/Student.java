package ru.b3nder;

import java.util.Objects;

public class Student implements Comparable<Student> {
    private final String name;
    private final String surname;
    private final String patronymic;
    private final int studNumber;
    private final Gender gender;
    private final Passport passport;
    private Group group;

    public Student(String name, String surname, String patronymic, int studNumber, Gender gender, Passport passport) {
        this.name = name;
        this.surname = surname;
        this.patronymic = patronymic;
        this.studNumber = studNumber;
        this.gender = gender;
        this.passport = passport;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public int getStudNumber() {
        return studNumber;
    }

    public Gender getGender() {
        return gender;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public Passport getPassport() {
        return passport;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Student student = (Student) o;
        return studNumber == student.studNumber;
    }

    @Override
    public int hashCode() {
        return Objects.hash(studNumber);
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", patronymic='" + patronymic + '\'' +
                ", studNumber=" + studNumber +
                ", gender=" + gender +
                ", passport=" + passport +
                '}';
    }

    @Override
    public int compareTo(Student student) {
        return surname.compareTo(student.getSurname());
    }
}
