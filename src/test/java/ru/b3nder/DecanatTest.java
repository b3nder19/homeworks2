package ru.b3nder;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class DecanatTest {
    Decanat decanat = new Decanat();

    Passport passport1 = new Passport(1234, 123456);
    Passport passport2 = new Passport(4321, 654321);

    Student student = decanat.createStudent("Олег", "Тимошенко", "Владимирович",
            Gender.MALE, passport1);
    Student student2 = decanat.createStudent("Саня", "Григоренко", "Батькович",
            Gender.MALE, passport2);

    Group group = decanat.createGroup("Гражданское строительство");
    Group group2 = decanat.createGroup("Промышленное гражданское строительство");


    @Test
    public void getAllStudent() {
        List<Student> expected = decanat.getAllStudents();

        List<Student> actual = new ArrayList<>();
        actual.add(student);

        assertEquals(expected, actual);
    }

    @Test
    public void getAllStudent_NO_NULL() {
        List<Student> expected = decanat.getAllStudents();

        assertNotNull(expected);
    }

    @Test
    public void addStudentToGroup() {
        assertTrue(decanat.addStudent(group, student));
    }

    @Test
    public void getStudentsByGroup() {
        decanat.addStudent(group, student);
        List<Student> actualStudent = new ArrayList<>();

        actualStudent.add(student);

        assertEquals(decanat.getStudentsByGroup(group), actualStudent);
    }

    @Test
    public void moveStudent() {
        decanat.addStudent(group, student);
        assertTrue(decanat.moveStudent(student, group2));
    }

    @Test
    public void kickStudent() {
        decanat.kickStudent(student);
    }

    @Test
    public void getGroups() {
        List<Group> groupList = new ArrayList<>();
        groupList.add(group);
        groupList.add(group2);

        assertEquals(groupList, decanat.getGroups());
    }

    @Test
    public void sizeOfGroup() {
        decanat.addStudent(group, student);

        List<Student> actualSizeStudents = new ArrayList<>();
        actualSizeStudents.add(student);

        assertEquals(actualSizeStudents, decanat.getStudentsByGroup(group));
    }

    @Test
    public void isStudentExist() {
        assertTrue(decanat.isStudentExist("Олег", "Тимошенко", "Владимирович"));
    }

    @Test
    public void getPassData() {
        assertEquals(passport1, decanat.getPassData(student));
    }

    @Test
    public void printGroup() {
        decanat.addStudent(group, student);
        decanat.addStudent(group, student2);


    }
}

